package ebay.http;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JsonMapper {

    @Bean
    ObjectMapper createJsonMapper(){
        return new ObjectMapper();
    }
}
