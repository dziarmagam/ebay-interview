package ebay.catalog;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import ebay.domain.CatalogMapping;

@Component
public class CatalogServiceImpl implements CatalogService {
    private final List<CatalogMapping> catalogMappings;

    public CatalogServiceImpl() throws IOException {
        this.catalogMappings = loadAllEntities();
    }

    @Override
    public List<CatalogMapping> findBy(Integer catalogId, Integer siteID) {
        if (catalogId == -1) {
            return catalogMappings.stream().filter(it -> it.getSiteID() == siteID).collect(Collectors.toList());
        } else {
            return catalogMappings.stream().filter(it -> it.getSiteID() == siteID && it.getCatalogId() == catalogId).collect(Collectors.toList());
        }
    }

    private List<CatalogMapping> loadAllEntities() throws IOException {
        return Files.lines(Paths.get("catalog-mapping-data.tsv"))
                .map(it -> it.split("\t"))
                .map(it -> new CatalogMapping(Integer.parseInt(it[0]), Long.parseLong(it[1]), Long.parseLong(it[2]), it[3]))
                .collect(Collectors.toList());
    }
}
