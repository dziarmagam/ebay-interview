package ebay.catalog;

import java.util.List;

import ebay.domain.CatalogMapping;

public interface CatalogService {
    List<CatalogMapping> findBy(Integer catalogId, Integer siteID);
}
