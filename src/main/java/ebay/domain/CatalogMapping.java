package ebay.domain;

import java.util.Objects;

public class CatalogMapping {
    private final int siteID;
    private final long categoryId;
    private final long catalogId;
    private final String categoryName;

    public CatalogMapping(int siteID, long categoryId, long catalogId, String categoryName) {
        this.siteID = siteID;
        this.categoryId = categoryId;
        this.catalogId = catalogId;
        this.categoryName = categoryName;
    }

    public int getSiteID() {
        return siteID;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public long getCatalogId() {
        return catalogId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatalogMapping that = (CatalogMapping) o;
        return siteID == that.siteID && categoryId == that.categoryId && catalogId == that.catalogId && Objects.equals(categoryName, that.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(siteID, categoryId, catalogId, categoryName);
    }
}
