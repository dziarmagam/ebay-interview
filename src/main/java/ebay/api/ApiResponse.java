package ebay.api;

import java.util.List;
import java.util.Objects;

import ebay.domain.CatalogMapping;

public class ApiResponse{
    private ResponseStatus responseStatus;
    private List<String> errors;
    private List<CatalogMapping> catalogMappingList;

    public ApiResponse(ResponseStatus responseStatus, List<String> errors, List<CatalogMapping> catalogMappingList) {
        this.responseStatus = responseStatus;
        this.errors = errors;
        this.catalogMappingList = catalogMappingList;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<CatalogMapping> getCatalogMappingList() {
        return catalogMappingList;
    }

    public void setCatalogMappingList(List<CatalogMapping> catalogMappingList) {
        this.catalogMappingList = catalogMappingList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiResponse that = (ApiResponse) o;
        return responseStatus == that.responseStatus && Objects.equals(errors, that.errors) && Objects.equals(catalogMappingList, that.catalogMappingList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(responseStatus, errors, catalogMappingList);
    }
}