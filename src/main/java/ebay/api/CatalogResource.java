package ebay.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import ebay.catalog.CatalogService;
import ebay.domain.CatalogMapping;

@RestController
@RequestMapping("/catalogmapping")
public class CatalogResource {

    private final CatalogService catalogService;
    private final ObjectMapper objectMapper;


    @Autowired
    public CatalogResource(CatalogService catalogService, ObjectMapper objectMapper) {
        this.catalogService = catalogService;
        this.objectMapper = objectMapper;
    }


    @GetMapping(value = "/getcatalogmapping/json/{catalogId}/{siteID}", produces = "application/json")
    ResponseEntity<String> getCatalogMapping(@PathVariable Integer catalogId, @PathVariable Integer siteID) throws JsonProcessingException {
        List<CatalogMapping> catalogMappings = catalogService.findBy(catalogId, siteID);
        ApiResponse listApiResponse = new ApiResponse(ResponseStatus.SUCCESS, Collections.emptyList(), catalogMappings);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("content-type", "application/json");
        return new ResponseEntity<>(objectMapper.writeValueAsString(listApiResponse), httpHeaders, HttpStatus.OK);
    }

}
