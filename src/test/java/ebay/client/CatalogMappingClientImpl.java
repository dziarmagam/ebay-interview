package ebay.client;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CatalogMappingClientImpl implements CatalogMappingClient {


    private static final String mappingByCatalogUrlTemplate = "/catalogmapping/getcatalogmapping/json/%d/%d";
    private final OkHttpClient client;
    private final String baseUrl;
    private final ObjectMapper objectMapper;

    public CatalogMappingClientImpl(OkHttpClient client, String baseUrl, ObjectMapper objectMapper) {
        this.client = client;
        this.baseUrl = baseUrl;
        this.objectMapper = objectMapper;
    }


    @Override
    public CatalogMappingResponse getMappingByCatalogAndSite(long catalogId, int siteId) {

        String url = baseUrl + String.format(mappingByCatalogUrlTemplate, catalogId, siteId);
        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            Response execute = client.newCall(request).execute();
            if (execute.isSuccessful()) {

                return objectMapper.readValue(execute.body().string(), CatalogMappingResponse.class);
            } else {
                throw new ResponseNotSuccessful(execute.body().string(), execute.code());
            }
        } catch (IOException e) {
            throw new ExceptionWhileCallingEndpoint(url, e);
        }

    }

    class ExceptionWhileCallingEndpoint extends RuntimeException {
        private static final String exceptionTemplate = "Exception while calling endpoint %s";
        public final String endpointUrl;

        ExceptionWhileCallingEndpoint(String endpointUrl, Exception e) {
            super(String.format(exceptionTemplate, endpointUrl), e);
            this.endpointUrl = endpointUrl;
        }
    }

    class ResponseNotSuccessful extends RuntimeException {
        private static final String exceptionTemplate = "Response was not successful, code: %s, body: %s";

        public final String body;
        public final int responseCode;

        ResponseNotSuccessful(String body, int responseCode) {
            super(String.format(exceptionTemplate, responseCode, body));
            this.body = body;
            this.responseCode = responseCode;
        }
    }
}
