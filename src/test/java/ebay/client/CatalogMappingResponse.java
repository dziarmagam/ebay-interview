package ebay.client;

import java.util.List;

public class CatalogMappingResponse {
    private String responseStatus;
    private List<String> errors;
    private List<CatalogMapping> catalogMappingList;

    public CatalogMappingResponse() {
    }

    public CatalogMappingResponse(String responseStatus, List<String> errors, List<CatalogMapping> catalogMappingList) {
        this.responseStatus = responseStatus;
        this.errors = errors;
        this.catalogMappingList = catalogMappingList;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<CatalogMapping> getCatalogMappingList() {
        return catalogMappingList;
    }

    public void setCatalogMappingList(List<CatalogMapping> catalogMappingList) {
        this.catalogMappingList = catalogMappingList;
    }

    @Override
    public String toString() {
        return "CatalogMappingResponse{" +
                "responseStatus='" + responseStatus + '\'' +
                ", errors=" + errors +
                ", catalogMappingList=" + catalogMappingList +
                '}';
    }
}

