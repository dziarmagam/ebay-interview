package ebay.client;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import okhttp3.OkHttpClient;

public class CatalogMappingClientImplTest {

    @Test
    void should_call_catalog_mapping_endpoint_and_return_valid_response_object(){
        //given
        OkHttpClient okHttpClient = new OkHttpClient();
        String baseUrl = "http://localhost:8080/";
        ObjectMapper objectMapper = new ObjectMapper();
        CatalogMappingClientImpl catalogMappingClient = new CatalogMappingClientImpl(okHttpClient, baseUrl, objectMapper);
        //when
        CatalogMappingResponse mappingByCatalogAndSite = catalogMappingClient.getMappingByCatalogAndSite(1L, 0);
        //then
        System.out.println(mappingByCatalogAndSite);

    }

}
