package ebay.client;

public interface CatalogMappingClient {
    CatalogMappingResponse getMappingByCatalogAndSite(long catalogId, int siteId);
}
